import java.util.ArrayList;

public class Main {


    public static void main(String[] args){
        LoadData easyLesson = new LoadData();
        easyLesson.load("./res/lessonMediumData.json");
        ArrayList<Lesson> lessonList = easyLesson.getLessonList();

        System.out.println(lessonList.get(0).getLessonID());
        System.out.println(lessonList.get(0).getDifficulty());
        System.out.println(lessonList.get(0).getUse());
        System.out.println(lessonList.get(0).getExampleCN1());
        System.out.println(lessonList.get(0).getExamplePN1());
        System.out.println(lessonList.get(0).getExampleEN1());
        System.out.println(lessonList.get(0).getExampleCN2());
        System.out.println(lessonList.get(0).getExamplePN2());
        System.out.println(lessonList.get(0).getExampleEN2());
        System.out.println(lessonList.get(0).getImg1());
        System.out.println(lessonList.get(0).getImg2());
    }


}
