import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class LoadData {

    private ArrayList<Lesson> lessonList = new ArrayList<>();
    private String fileName;

    public LoadData() {
    }

    public void load(String fileName)
    {
        JSONParser parser = new JSONParser();
        try
        {
            Object object = parser
                    .parse(new FileReader(fileName));

            //convert Object to JSONObject
            JSONObject jsonObject = (JSONObject)object;

            //Reading the array
            JSONArray lessons = (JSONArray)jsonObject.get("lessons");

            //Create the objects
            for(Object lessonObj : lessons){
                JSONObject lessonJSON = (JSONObject) lessonObj;
                lessonList.add(new Lesson(
                        (String)lessonJSON.get("lessonID"),
                        (String)lessonJSON.get("Difficulty"),
                        (String)lessonJSON.get("nameCN"),
                        (String)lessonJSON.get("nameEN"),
                        (String)lessonJSON.get("use"),
                        (String)lessonJSON.get("exampleCN1"),
                        (String)lessonJSON.get("examplePN1"),
                        (String)lessonJSON.get("exampleEN1"),
                        (String)lessonJSON.get("exampleCN2"),
                        (String)lessonJSON.get("examplePN2"),
                        (String)lessonJSON.get("exampleEN2"),
                        (String)lessonJSON.get("img1"),
                        (String)lessonJSON.get("img2")));
            }
        }
        catch(FileNotFoundException fe)
        {
            System.err.println("FILE NOT FOUND!");;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }



//        public void loadEasy()
//        {
//            JSONParser parser = new JSONParser();
//            try
//            {
//                Object object = parser
//                        .parse(new FileReader("./res/lessonEasyData.json"));
//
//                //convert Object to JSONObject
//                JSONObject jsonObject = (JSONObject)object;
//
//                //Reading the array
//                JSONArray lessons = (JSONArray)jsonObject.get("lessons");
//
//                //Create the objects
//                for(Object lessonObj : lessons){
//                    JSONObject lessonJSON = (JSONObject) lessonObj;
//                    lessonList.add(new Lesson(
//                            (String)lessonJSON.get("lessonID"),
//                            (String)lessonJSON.get("Difficulty"),
//                            (String)lessonJSON.get("nameCN"),
//                            (String)lessonJSON.get("nameEN"),
//                            (String)lessonJSON.get("use"),
//                            (String)lessonJSON.get("exampleCN1"),
//                            (String)lessonJSON.get("examplePN1"),
//                            (String)lessonJSON.get("exampleEN1"),
//                            (String)lessonJSON.get("exampleCN2"),
//                            (String)lessonJSON.get("examplePN2"),
//                            (String)lessonJSON.get("exampleEN2"),
//                            (String)lessonJSON.get("img1"),
//                            (String)lessonJSON.get("img2")));
//                }
//            }
//            catch(FileNotFoundException fe)
//            {
//                System.err.println("FILE NOT FOUND!");;
//            }
//            catch(Exception e)
//            {
//                e.printStackTrace();
//            }



    }







    public ArrayList<Lesson> getLessonList() {
        return lessonList;
    }
}
